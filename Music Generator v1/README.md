# Music Generator v1

## Introduction

I play the piano, but I don't know much about writing music. I'm going
to look at tutorials on YouTube and other places to figure out how
to structure basic songs, and then try to algorithmically create
basic songs.

## Song structure
* Verse | Chorus | Verse | Chorus
* Verse 1 | Chorus 1 | Verse 2 | Chorus 2 | Bridge | Chorus 3

Usually a verse is like several lines of lyrics (16 or 32 bars)



## Resources

* [Song structure - Wikipedia](https://en.wikipedia.org/wiki/Song_structure)
* [HELP! What's a VERSE, CHORUS, & BRIDGE? (Songwriting 101) - Dylan Laine](https://www.youtube.com/watch?v=hE_qOY5GkH0)
* [Anatomy of a Song: The Three Most Common Song Forms](https://www.musical-u.com/learn/anatomy-of-a-song-the-three-most-common-song-forms/)
