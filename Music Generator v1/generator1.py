import pygame, sys, random
from pygame.locals import *

pygame.init()
fpsClock = pygame.time.Clock()

window = pygame.display.set_mode( ( 640, 100 ) )
pygame.display.set_caption( "Random song" )
pygame.time.set_timer( pygame.USEREVENT, 1000 )

sounds = {
	"guitar" : {
		"C" : pygame.mixer.Sound( "audio/guitar-C.ogg" ),
		"D" : pygame.mixer.Sound( "audio/guitar-D.ogg" ),
		"E" : pygame.mixer.Sound( "audio/guitar-E.ogg" ),
		"F" : pygame.mixer.Sound( "audio/guitar-F.ogg" ),
		"G" : pygame.mixer.Sound( "audio/guitar-G.ogg" ),
		"A" : pygame.mixer.Sound( "audio/guitar-A.ogg" ),
		"B" : pygame.mixer.Sound( "audio/guitar-B.ogg" ),
		"C2" : pygame.mixer.Sound( "audio/guitar-C2.ogg" )
	},
	"bass" : {
		"C" : pygame.mixer.Sound( "audio/bass-C.ogg" ),
		"D" : pygame.mixer.Sound( "audio/bass-D.ogg" ),
		"E" : pygame.mixer.Sound( "audio/bass-E.ogg" ),
		"F" : pygame.mixer.Sound( "audio/bass-F.ogg" ),
		"G" : pygame.mixer.Sound( "audio/bass-G.ogg" ),
		"A" : pygame.mixer.Sound( "audio/bass-A.ogg" ),
		"B" : pygame.mixer.Sound( "audio/bass-B.ogg" ),
		"C2" : pygame.mixer.Sound( "audio/bass-C2.ogg" )
	}
}

sounds["guitar"]["C"].play()

bg = pygame.Color( 255, 200, 200 )

# Verse 1 | Chorus 1 | Verse 2 | Chorus 2 | Bridge | Chorus 3
# 

def GetRandomNote():
	num = random.randrange( 0, 8 )
	
	if 		( num == 0 ): return "C"
	elif 	( num == 1 ): return "D"
	elif 	( num == 2 ): return "E"
	elif 	( num == 3 ): return "F"
	elif 	( num == 4 ): return "G"
	elif 	( num == 5 ): return "A"
	elif 	( num == 6 ): return "B"
	elif 	( num == 7 ): return "C2"
	
	return "C"
	

def GenerateSongString( existing, noteCount, instrument ):
	print( "Generate" )
	
	if ( existing == None ):
		print( "Generate new" )
		phrase = []
		for i in range( noteCount ):
			note = GetRandomNote()
			print( "Add " + note )
			
			phrase.append( sounds[instrument][note] )
		
	else:
		print( "Modify old" )
		phrase = existing
		changeCount = random.randrange( 0, int( len( existing ) / 2 ) )
		for c in range( changeCount ):
			# Change note c
			note = GetRandomNote()
			print( "Change note " + str( c ) + " to " + note )
			phrase[c] = sounds[instrument][note]			
			
	return phrase

def GenerateBridge():
	phrase = []
	
	return ""
	
	
songCounter = 0
	
songMelody = []

firstVerse = GenerateSongString( None, 4, "guitar" )
firstChorus = GenerateSongString( None, 4, "guitar" )

songMelody.extend( firstVerse ) # Verse
songMelody.extend( firstChorus ) # Chorus
songMelody.extend( GenerateSongString( firstVerse, 4, "guitar" ) ) # Verse
songMelody.extend( GenerateSongString( firstChorus, 4, "guitar" ) ) # Chorus
songMelody.extend( GenerateSongString( None, 4, "guitar" ) ) # Bridge
songMelody.extend( GenerateSongString( firstChorus, 4, "guitar" ) ) # Chorus

songAccompany = []

firstVerse = GenerateSongString( None, 4, "bass" )
firstChorus = GenerateSongString( None, 4, "bass" )

songAccompany.extend( firstVerse ) # Verse
songAccompany.extend( firstChorus ) # Chorus
songAccompany.extend( GenerateSongString( firstVerse, 4, "bass" ) ) # Verse
songAccompany.extend( GenerateSongString( firstChorus, 4, "bass" ) ) # Chorus
songAccompany.extend( GenerateSongString( None, 4, "bass" ) ) # Bridge
songAccompany.extend( GenerateSongString( firstChorus, 4, "bass" ) ) # Chorus


while True:
	window.fill( bg )
	
	for event in pygame.event.get():
		if ( event.type == QUIT ):
			pygame.quit()
			sys.exit()
			
		if ( event.type == pygame.USEREVENT ):
			print( songCounter )
			
			songMelody[ songCounter ].play()
			songAccompany[ songCounter ].play()
			songCounter += 1
			if ( songCounter >= len( songMelody ) ):
				songCounter = 0;
			
	pygame.display.update()
	fpsClock.tick( 30 )







